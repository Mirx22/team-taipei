﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceVisualization
{
    public class QueryStore
    {
        List<Process> Processes;

        public QueryStore(List<Process> _processes)
        {
            Processes = _processes;
        }

        public IEnumerable<Process> QueryMaxHandleCount()
        {
            return Processes.OrderByDescending(x => x.HCPeak).GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
        }

        public IEnumerable<Process> QueryMaxThreadCount()
        {
            return Processes.OrderByDescending(x => x.TCPeak).GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
        }

        public IEnumerable<Process> QueryMaxCPU()
        {
            return Processes.OrderByDescending(x => x.CPUPeak).GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
        }

        public IEnumerable<Process> QueryColumnValue(string value)
        {
            return Processes.Where(x => x.Name == value).OrderBy(x => x.Timestamp);
        }

        public IEnumerable<Process> QueryDateTime(List<Process> singleProcessList, DateTime start, DateTime end)
        {
            return singleProcessList.Where(x => x.Timestamp >= start)
                            .Where(x => x.Timestamp <= end);
        }
    }
}
