﻿using LiveCharts;
using LiveCharts.Wpf;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace PerformanceVisualization
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        public const string HC_PEAK_NAME = "HCPeak";
        public const string TC_PEAK_NAME = "TCPeak";
        public const string CPU_PEAK_NAME = "CPUPeak";
        public List<Process> Processes = new List<Process>();
        public QueryStore QueryStore;
        private ChartWrapper hc;
        private ChartWrapper tc;
        private ChartWrapper cpu;

        public MainWindow()
        {
            InitializeComponent();
            queryBox.SelectionChanged += PerformQuery;
        }

        private void AddFileTile_Click(object sender, RoutedEventArgs e)
        {
            LoadFile(false);
        }

        private void LoadResultTile_Click(object sender, RoutedEventArgs e)
        {
            LoadFile(true);
        }

        private void SaveFileTile_Click(object sender, RoutedEventArgs e)
        {
            if (TabControl == null || QueryStore == null || TabControl.SelectedItem == null)
            {
                // maybbe show message "need to load stuff or somethhing"
                return;
            }

            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog()
            {
                AddExtension = true,
                DefaultExt = ".utr"
            };

            var result = saveFileDialog.ShowDialog();

            if (result == true)
            {
                ChartWrapper chartWrapper = null;
                switch ((TabControl.SelectedItem as TabItem).Name)
                {
                    case "HC": chartWrapper = hc; break;
                    case "TC": chartWrapper = tc; break;
                    case "CPU": chartWrapper = cpu; break;
                }

                if (chartWrapper != null)
                {
                    File.WriteAllLines(
                        saveFileDialog.FileName,
                        new string[] { "TraceFormat:%TIME%|%PID%|%TID%|%LEVEL%|%USERTEXT%" }
                        .Concat(chartWrapper.processesSegment.Select(x => x.Raw))
                    );
                }
            }
        }

        private void MemoryTile_Click(object sender, RoutedEventArgs e)
        {
            if (Processes.Count <= 0)
            {
                MessageBox.Show("Load a tracefile first!", "Performance Visualization");
            }
            else
            {
                CreateTable(QueryStore.QueryMaxThreadCount());
            }
        }

        private void HandlesTile_Click(object sender, RoutedEventArgs e)
        {
            if (Processes.Count <= 0)
            {
                MessageBox.Show("Load a tracefile first!", "Performance Visualization");
            }
            else
            {
                CreateTable(QueryStore.QueryMaxHandleCount());
            }
        }

        private void CpuTile_Click(object sender, RoutedEventArgs e)
        {
            if (Processes.Count <= 0)
            {
                MessageBox.Show("Load a tracefile first!", "Performance Visualization");
            }
            else
            {
                CreateTable(QueryStore.QueryMaxCPU());
            }
        }

        private void PerformQuery(object sender, SelectionChangedEventArgs e)
        {
            if (Processes.Count <= 0)
            {
                MessageBox.Show("Load a tracefile first!", "Performance Visualization");
            }
            else
            {
                if (e.AddedItems.Count > 0)
                {
                    InitAndShowTables(e.AddedItems[0].ToString().Trim());
                }
                else
                {
                    InitAndShowTables(string.Empty);
                }
            }
        }

        private void CreateTable(IEnumerable<Process> _processes)
        {
            var nameBinding = new Binding("Name")
            {
                Mode = BindingMode.Default,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };
            var pidBinding = new Binding("PID")
            {
                Mode = BindingMode.Default,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };
            var cpuBinding = new Binding("CPUPeak")
            {
                Mode = BindingMode.Default,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };
            var tcBinding = new Binding("TCPeak")
            {
                Mode = BindingMode.Default,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };
            var hcBinding = new Binding("HCPeak")
            {
                Mode = BindingMode.Default,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };
            var dataGrid = new DataGrid();
            dataGrid.Columns.Add(new DataGridTextColumn
            {
                Header = "Process Name",
                Binding = nameBinding,
                Width = 400
            });
            dataGrid.Columns.Add(new DataGridTextColumn
            {
                Header = "Process ID",
                Binding = pidBinding,
                Width = 100
            });
            dataGrid.Columns.Add(new DataGridTextColumn
            {
                Header = "CPU Peak",
                Binding = cpuBinding,
                Width = 100
            });
            dataGrid.Columns.Add(new DataGridTextColumn
            {
                Header = "TC Peak",
                Binding = tcBinding,
                Width = 100
            });
            dataGrid.Columns.Add(new DataGridTextColumn
            {
                Header = "HC Peak",
                Binding = hcBinding,
                Width = 100
            });
            dataGrid.ItemsSource = _processes.ToList();
            dataGrid.MouseDoubleClick += DataGrid_MouseDoubleClick;
            dataGrid.Margin = new Thickness(0, 10, 0, 0);
            dataGrid.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            dataGrid.AutoGenerateColumns = false;
            dataGrid.IsReadOnly = true;
            AppGrid.Children.Clear();
            AppGrid.Children.Add(dataGrid);
            AppGrid.Children.Add(TabControl);
        }

        private void DataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is DataGrid dataGrid && dataGrid.SelectedItem != null && dataGrid.SelectedItem is Process process)
            {
                if (Processes.Count <= 0)
                {
                    MessageBox.Show("Load a tracefile first!", "Performance Visualization");
                }
                else
                {
                    InitAndShowTables(process.Name);
                }
            }
        }

        void LoadFile(bool forceSameWindow)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            string line = "";

            dialog.DefaultExt = ".utr";
            dialog.Filter = "UTR Files (*.utr)|*.utr";

            bool? result = dialog.ShowDialog();

            if (result == true)
            {
                StreamReader streamReader = new StreamReader(dialog.FileName);
                TracefileParser tracefileParser = new TracefileParser();

                var processes = new List<Process>();

                while (line != null)
                {
                    line = streamReader.ReadLine();
                    if (line != null)
                    {
                        if (line.IndexOf("Process: ") >= 0)
                        {
                            processes.Add(tracefileParser.ParseLineToProcess(line));
                        }
                    }
                }
                streamReader.Close();

                if (QueryStore == null || forceSameWindow)
                {
                    Processes = processes;
                    QueryStore = new QueryStore(processes);

                    queryBox.ItemsSource = processes.Select(x => x.Name).Distinct();

                    MessageBox.Show("Tracefile successfully loaded!", "Performance Visualization");
                }
                else
                {
                    var newmain = new MainWindow();
                    newmain.Processes = processes;
                    newmain.QueryStore = new QueryStore(processes);

                    newmain.queryBox.ItemsSource = processes.Select(x => x.Name).Distinct();

                    newmain.Show();
                }
            }
        }

        void InitAndShowTables(string procName)
        {
            var selectedProcesses = QueryStore.QueryColumnValue(procName).ToArray();

            hc = new ChartWrapper(selectedProcesses, x => x.HCPeak, HCGrid, "HCPeak");
            tc = new ChartWrapper(selectedProcesses, x => x.TCPeak, TCGrid, "TCPeak");
            cpu = new ChartWrapper(selectedProcesses, x => x.CPUPeak / 100d, CPUGrid, "CPUPeak");

            TabControl.Visibility = Visibility.Visible;
        }

        class ChartWrapper
        {
            public ArraySegment<Process> processesSegment;

            private readonly TextBox textBoxFrom;
            private readonly TextBox textBoxTo;
            private readonly CheckBox hotspot;

            private DateTime? _manualFrom;
            public DateTime? From
            {
                get => _manualFrom ?? processes.First().Timestamp;
                set
                {
                    _manualFrom = value;
                    textBoxFrom.Text = value.ToString();
                    _rescopeGraph();
                }
            }
            private DateTime? _manualTo;
            public DateTime? To
            {
                get => _manualTo ?? processes.Last().Timestamp;
                set
                {
                    _manualTo = value;
                    textBoxTo.Text = value.ToString();
                    _rescopeGraph();
                }
            }

            List<ChartPoint> selectedPoints = new List<ChartPoint>();
            readonly CartesianChart ch;
            private readonly Process[] processes;
            private readonly Func<Process, double> selector;
            private readonly string title;

            public ChartWrapper(Process[] processes, Func<Process, double> selector, Grid target, string title, int w = 800, int h = 600)
            {
                this.processes = processes;
                this.selector = selector;
                this.title = title;
                this.processesSegment = new ArraySegment<Process>(processes, 0, processes.Length);

                var values = new ChartValues<double>(this.processesSegment.Select(selector));

                //HorizontalAlignment = "Left" Height = "23" TextWrapping = "Wrap" Text = "TextBox" VerticalAlignment = "Top" Width = "120" Margin = "125,0,0,0"
                textBoxFrom = new TextBox
                {
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Height = 23,
                    Width = 120,
                    TextWrapping = TextWrapping.Wrap,
                    Margin = new Thickness(0, 0, 0, 0),
                };
                textBoxTo = new TextBox
                {
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Height = 23,
                    Width = 120,
                    TextWrapping = TextWrapping.Wrap,
                    Margin = new Thickness(125, 0, 0, 0)
                };

                //< CheckBox Content = "CheckBox" HorizontalAlignment = "Left" Margin = "173,57,0,0" VerticalAlignment = "Top" />
                hotspot = new CheckBox
                {
                    Content = "Hotspot",
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(250, 0, 0, 0)
                };

                textBoxFrom.TextChanged += (sender, e) =>
                {
                    if (DateTime.TryParse(textBoxFrom.Text, out DateTime value))
                    {
                        From = value;
                    }
                    else if (string.IsNullOrWhiteSpace(textBoxFrom.Text))
                    {
                        From = null;
                    }
                };
                textBoxTo.TextChanged += (sender, e) =>
                {
                    if (DateTime.TryParse(textBoxTo.Text, out DateTime value))
                    {
                        To = value;
                    }
                    else if (string.IsNullOrWhiteSpace(textBoxTo.Text))
                    {
                        To = null;
                    }
                };
                hotspot.Checked += (sender, e) => _rescopeGraph();
                hotspot.Unchecked += (sender, e) => _rescopeGraph();

                ch = new CartesianChart
                {
                    Series = new SeriesCollection
                    {
                        new LineSeries
                        {
                            Title = title,
                            Values = values,
                            LabelPoint = x => $"{this.processesSegment.Skip(x.Key).First().Timestamp.ToString()}: {x.Y}"
                        }
                    },
                    Width = w,
                    Height = h,
                    Margin = new Thickness(0, 25, 0, 0)
                };

                target.Children.Clear();
                target.Children.Add(textBoxFrom);
                target.Children.Add(textBoxTo);
                target.Children.Add(hotspot);
                target.Children.Add(ch);

                ch.DataClick += _DataClick;
            }

            private void _DataClick(object sender, ChartPoint chartPoint)
            {
                if (selectedPoints.Count < 2)
                {
                    selectedPoints.Add(chartPoint);
                }
                else
                {
                    selectedPoints.RemoveAt(0);
                    selectedPoints.Add(chartPoint);
                }

                if (selectedPoints.Count == 2)
                {
                    var ordered = selectedPoints.OrderBy(x => x.X);

                    var timestampFrom = processes[processesSegment.Offset + ordered.First().Key].Timestamp;
                    var timestampTo = processes[processesSegment.Offset + ordered.Last().Key].Timestamp;

                    From = timestampFrom;
                    To = timestampTo;

                    selectedPoints.Clear();
                }
            }

            private void _rescopeGraph()
            {
                var fromsFirst = processes.FirstOrDefault(x => x.Timestamp >= From);
                var tosLast = processes.LastOrDefault(x => x.Timestamp <= To);

                ChartValues<double> values;

                if (fromsFirst != null && tosLast != null && fromsFirst.Timestamp <= tosLast.Timestamp)
                {
                    int fromsIdx = Array.IndexOf(processes, fromsFirst);
                    int tosIdx = Array.IndexOf(processes, tosLast);

                    processesSegment = new ArraySegment<Process>(processes, fromsIdx, tosIdx - fromsIdx + 1);

                    values = hotspot.IsChecked ?? false
                        ? new ChartValues<double>(processesSegment.Where(p => p.CPUPeak >= 8000).Select(selector))
                        : new ChartValues<double>(processesSegment.Select(selector));
                }
                else
                {
                    values = new ChartValues<double>(Array.Empty<double>());
                }

                ch.Series = new SeriesCollection
                    {
                        new LineSeries
                        {
                            Title = title,
                            Values = values,
                            LabelPoint = x =>  $"{this.processesSegment.Skip(x.Key).First().Timestamp.ToString()}: {x.Y}"
                        }
                    };
            }
        }
    }
}
