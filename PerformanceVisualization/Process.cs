﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceVisualization
{
    public class Process
    {
        public string Name { get; set; }
        public int PID { get; set; }
        public int HCPeak { get; set; }
        public int TCPeak { get; set; }
        public double CPUPeak { get; set; }
        public DateTime Timestamp { get; set; }

        public string Raw { get; set; }

        public override string ToString()
        {
            return "Process Name: " + Name + "; HCPeak: " + HCPeak + "; TCPeak: " + TCPeak + "; CPUPeak: " + CPUPeak + "%; Timestamp: " + Timestamp;
        }
    }
}
