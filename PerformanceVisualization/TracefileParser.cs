﻿using System;

namespace PerformanceVisualization
{
    class TracefileParser
    {

        public const string HcPeakName = "HCPeak";
        public const string TcPeakName = "TCPeak";
        public const string CpuPeakName = "CPUPeak";

        public Process ParseLineToProcess(string line)
        {
            string name = line.Substring(GetNthIndex(line, ':', 3) + 1);
            name = name.Substring(0, name.IndexOf('('));
            DateTime timestamp = DateTime.ParseExact(line.Substring(0, line.IndexOf("|")), "yyyy/MM/dd-HH:mm:ss.ffffff", null);
            string hcpeak = ProcessInfoParser(line, HcPeakName);
            string tcpeak = ProcessInfoParser(line, TcPeakName);
            string cpupeak = ProcessInfoParser(line, CpuPeakName);
            return new Process
            {
                Name = name.Trim(),
                Timestamp = timestamp,
                HCPeak = Int32.Parse(hcpeak),
                TCPeak = Int32.Parse(tcpeak),
                CPUPeak = double.Parse(cpupeak),
                Raw = line
            };
        }

        private string ProcessInfoParser(string line, string processName)
        {
            string infoValue = line.Substring(line.IndexOf(processName));
            infoValue = infoValue.Substring(infoValue.IndexOf(':') + 2);
            infoValue = infoValue.Substring(0, infoValue.IndexOf(';'));
            bool isNumeric = double.TryParse(infoValue, out double n);
            if (!isNumeric)
            {
                infoValue = infoValue.Remove(infoValue.Length - 1);
            }
            return ForceToZero(infoValue);
        }

        private int GetNthIndex(string s, char t, int n)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == t)
                {
                    count++;
                    if (count == n)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        private string ForceToZero(string value)
        {
            if (value.Contains("n.a"))
            {
                return "0";
            }
            else
            {
                return value;
            }
        }
    }
}
